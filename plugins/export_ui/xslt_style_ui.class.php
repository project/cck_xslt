<?php

class xslt_style_ui extends ctools_export_ui {
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Style description'),
      '#description' => t('This description will appear on the Style administrative UI to tell you what the style is about.'),
      '#default_value' => $form_state['item']->description,
    );

    $form['style'] = array(
      '#type' => 'textarea',
      '#title' => t('Style xslt'),
      '#description' => t('The xslt which is used to render the xml.'),
      '#default_value' => $form_state['item']->style,
    );
  }

  function edit_form_submit(&$form, &$form_state) {
    parent::edit_form_submit($form, $form_state);
    $form_state['item']->description = $form_state['values']['description'];
    $form_state['item']->style = $form_state['values']['style'];
  }
}