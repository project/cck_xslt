<?php

$plugin = array(
  'schema' => 'xslt_styles',

  'access' => 'administer xslt styles',

  'menu' => array(
    'menu prefix' => 'admin/build',
    'menu item' => 'xslt_styles',
    'menu title' => 'XSLT Styles', 
    'menu description' => 'Administer xslt styles.',
  ),

  'title singular' => t('xslt style'),
  'title singular proper' => t('XSLT Style'),
  'title plural' => t('xslt styles'),
  'title plural proper' => t('XSLT Styles'),

  'handler' => array(
    'class' => 'xslt_style_ui',
    'parent' => 'ctools_export_ui',
  ),
);