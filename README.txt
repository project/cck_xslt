-- SUMMARY --

The CCK Xslt module provides a cck field which converts xml from url/direct
input via xslt into html.


-- REQUIREMENTS --

Ctools, CCK


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- Configuration --
The xslt styles can be created on admin >> build >> XSLT Styles.
Once you have created your xslt styles you can add a new cck field to your content type.
There are two widgets availible
* XSLT from url
  This widget provides a textfield to enter the url of the xml.
* XSLT direct input
  This widget provides a textarea where you can input any kind of xml.
Additonal the user has to select one of the xslt styles.

At the end the xml is rendered with xslt. That's it.
